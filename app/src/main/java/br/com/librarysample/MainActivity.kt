package br.com.librarysample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.viacepsearch.data.ViaCepCaller
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        buscar.setOnClickListener {
            val rua = ViaCepCaller(searchCep.text.toString()).getAddress()
            address.text = "O seu logradouro é: ${rua}"
        }
    }
}
