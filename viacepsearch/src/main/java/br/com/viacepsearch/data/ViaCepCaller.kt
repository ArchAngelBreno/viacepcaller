package br.com.viacepsearch.data

import br.com.viacepsearch.data.entity.ViaSearchCep
import com.google.gson.GsonBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request

class ViaCepCaller(private val zipcode: String) {

    fun getAddress(): String? = runBlocking {
        withContext(Dispatchers.IO) { callAddress() }?.logradouro
    }

    private fun callAddress(): ViaSearchCep? {
        val okHttpClient = OkHttpClient.Builder().build()

        val request = Request.Builder()
            .get()
            .url("https://viacep.com.br/ws/$zipcode/json/")
            .build()
        try {
            val response = okHttpClient.newCall(request).execute()
            if (response.isSuccessful) {
                return GsonBuilder().create()
                    .fromJson(response.body?.string(), ViaSearchCep::class.java)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return null
    }
}
